//
// Created by aliciajgm on 25/01/21.
//

#include <Core/MotorHandler/EctoMotor/EctoGazeboMotor.h>
#include "Intake.h"
#include "frc/smartdashboard/SmartDashboard.h"
#include "Core/EctoInput/DefaultMappings.h"


Intake::Intake() {

	rightPiston = std::make_shared<EctoGazeboPiston>("Slimer2020_Offseason", "IntakePistonRight");
	leftPiston = std::make_shared<EctoGazeboPiston>("Slimer2020_Offseason", "IntakePistonLeft");
	rollers = std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "Intake");
	rollers->setEncoderCodesPerRev(42 * 2);
	rollers->setControlMode(MotorControlMode::Velocity);
	rollers->setFeedbackMode(MotorFeedbackMode::QuadEncoder);
	PIDConfig config;
	config.p = 0.0001;
	config.i = 0.0;
	config.d = 0.0;
	rollers->setPIDConfig(config);
	rollers->invertMotor(true);
	
	
}



void Intake::intakeDown() {
	rightPiston->setState(true);
	leftPiston->setState(true);
	
}

void Intake::intakeUp() {
	rightPiston->setState(false);
	leftPiston->setState(false);
	
}

void Intake::rollersVelocity(double velocityIntake) {
	rollers->setArbitraryFeedForward(velocityIntake * feedForward);
	rollers->set(velocityIntake);

}

double Intake::actualVelocity() {
	return rollers->getVelocity();
	
}

void Intake::config( PIDConfig configr){
	rollers->setPIDConfig(configr);
}
