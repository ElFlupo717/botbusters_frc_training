//
// Created by aliciajgm on 25/01/21.
//

#ifndef BOTBUSTERS_REBIRTH_INTAKE_H
#define BOTBUSTERS_REBIRTH_INTAKE_H

#include <Core/MotorHandler/EctoMotor/EctoMotor.h>
#include "Core/PCM//EctoPiston/EctoGazeboPiston.h"
#include "Core/EctoInput/Axis/JoystickAxisExpo.h"

class Intake {

public:
	Intake();
	
	void intakeDown() ;
	
	void intakeUp();
	
	void rollersVelocity(double velocityIntake);
	
	double actualVelocity();
	
	void config(PIDConfig configr);
	
private:
	
	std::shared_ptr<EctoPiston> rightPiston;
	std::shared_ptr<EctoPiston> leftPiston;
	std::shared_ptr<EctoMotor> rollers;
	const double feedForward = -0.1125;
};


#endif //BOTBUSTERS_REBIRTH_INTAKE_H

