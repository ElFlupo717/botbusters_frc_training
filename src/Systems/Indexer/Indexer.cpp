//
// Created by Dana Elizabeth Torres on 25/01/21.
//

#include "Indexer.h"

Indexer::Indexer() {
	midindexerMotor = std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "IndexerMid");
	midindexerMotor->setControlMode(MotorControlMode::Velocity);
	midindexerMotor->setFeedbackMode(MotorFeedbackMode::QuadEncoder);
	
	PIDConfig config;
	config.p = 0.6;
	midindexerMotor->setPIDConfig(config);
}

void Indexer::setVelocity(float velocity) {
	midindexerMotor->set(velocity);// -1000 a 1000
}
void Indexer::stop() {
	midindexerMotor->set(0.0);
}