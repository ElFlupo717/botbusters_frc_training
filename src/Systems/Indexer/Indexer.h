//
// Created by Dana Elizabeth Torres on 25/01/21.
//

#ifndef BOTBUSTERS_REBIRTH_INDEXER_H
#define BOTBUSTERS_REBIRTH_INDEXER_H
#include "Core/MotorHandler/EctoMotor/EctoGazeboMotor.h"

class Indexer {
public:
	Indexer();
	
	void setVelocity(float velocity);
	
	void stop();
	
protected:
	float velocity;
private:
	std::shared_ptr<EctoMotor> midindexerMotor;
};


#endif //BOTBUSTERS_REBIRTH_INDEXER_H
