//
// Created by tenzo on 1/26/21.
//

#ifndef BOTBUSTERS_REBIRTH_ECTOSHOOTER_H
#define BOTBUSTERS_REBIRTH_ECTOSHOOTER_H

#include "Core/PCM/EctoPiston/EctoGazeboPiston.h"
#include "Core/MotorHandler/EctoMotor/EctoGazeboMotor.h"
#include <stdexcept>
#include <frc/Timer.h>

class EctoShooter {
	public:
		EctoShooter();
		
		double getMotorVelocity();
		
		void update();
		
		void pistonState(bool extendedHood);

		void setVelocity(double velocityValue);
		
		bool isInWantedVelocity(double wantedVelocity);
		
		void PIDUpdater (PIDConfig config);
		
		void unJamMaths();
	
	private:
	double wheelRadius = 0.0508;
	
	double feedForward = 0.0;
	
	const double errorTolerance = 10.0;
	
	std::shared_ptr<EctoMotor> shooterMotor;
	std::shared_ptr<EctoPiston> hoodPiston;
};


#endif //BOTBUSTERS_REBIRTH_ECTOSHOOTER_H
