//
// Created by tenzo on 1/26/21.
//

#include "EctoShooter.h"

EctoShooter::EctoShooter() {
	PIDConfig config;
	
	config.p =      0.0300;
	config.i =      0.0000;
	config.d =      0.0001;
	feedForward =   2.0000;
	
	
	shooterMotor = std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "Shooter");
	hoodPiston = std::make_shared<EctoGazeboPiston>("Slimer2020_Offseason", "HoodPiston");
	
	shooterMotor->setPIDConfig(config);
	
	shooterMotor->setControlMode(MotorControlMode::Velocity);
	shooterMotor->setFeedbackMode(MotorFeedbackMode::QuadEncoder);
	shooterMotor->setEncoderCodesPerRev(42);
	
};

void EctoShooter::pistonState(bool extendedHood) {
	hoodPiston->setState(extendedHood);
};

void EctoShooter::setVelocity(double velocityValue) {
	
	double rpsValue = velocityValue / (wheelRadius * 2 * M_PI);
	shooterMotor->setArbitraryFeedForward(feedForward * velocityValue);
	shooterMotor->set(rpsValue);
};

double EctoShooter::getMotorVelocity() {
	return shooterMotor->getVelocity() * (wheelRadius * 2 * M_PI);
};

void EctoShooter::PIDUpdater(PIDConfig config) {
	shooterMotor->setPIDConfig(config);
	feedForward = config.f;
};


void EctoShooter::unJamMaths() {}

bool EctoShooter::isInWantedVelocity(double wantedVelocity) {
	
	double error = wantedVelocity - EctoShooter::getMotorVelocity();
	
	if (std::abs(error) < errorTolerance) {
		return true;
	} else {
		return false;
	}
}

void EctoShooter::update() {
	frc::Timer::GetFPGATimestamp();
}



