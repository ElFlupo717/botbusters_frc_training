//
// Created by andrew on 25/01/21.
//

#ifndef BOTBUSTERS_REBIRTH_ECTOSWERVE_H
#define BOTBUSTERS_REBIRTH_ECTOSWERVE_H

#include "Core/MotorHandler/EctoMotor/EctoGazeboMotor.h"
#include <frc/kinematics/SwerveDriveKinematics.h>
#include <frc/kinematics/SwerveDriveOdometry.h>
#include <frc/kinematics/SwerveModuleState.h>
#include "Sensors/GazeboImu.h"
#include "frc/GyroBase.h"
#include "frc/estimator/SwerveDrivePoseEstimator.h"
#include "frc/StateSpaceUtil.h"
#include <frc/system/plant/LinearSystemId.h>

class EctoSwerve {
public:
	EctoSwerve();
	
	void setVelocity(double vx, double vy, double omega, bool fieldOriented);
	
	frc::ChassisSpeeds getVelocity(bool fieldOriented);
	
	void update();
	
	frc::Pose2d getPose();

private:
	
	
	std::vector<std::shared_ptr<EctoMotor>> velocityMotors{
			
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "FrontRightWheel"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "FrontLeftWheel"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "BackRightWheel"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "BackLeftWheel")
	};
	
	std::vector<std::shared_ptr<EctoMotor>> rotationMotors{
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "FrontRightRotation"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "FrontLeftRotation"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "BackRightRotation"),
			std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason", "BackLeftRotation")
	};
	
	frc::SwerveDriveKinematics<4> m_kinematics{
			frc::Translation2d(0.28575_m, 0.26035_m),
			frc::Translation2d(0.28575_m, -0.26035_m),
			frc::Translation2d(-0.28575_m, 0.26035_m),
			frc::Translation2d(-0.28575_m, -0.26035_m)
	};
	
	frc::SwerveModuleState frontRightModuleState;
	frc::SwerveModuleState backRightModuleState;
	frc::SwerveModuleState frontLeftModuleState;
	frc::SwerveModuleState backLeftModuleState;
	
	double wheelRadius = 0.0508;
	
	GazeboIMU imu{"Slimer2020_Offseason", "imu"};
	
	frc::Pose2d pose;
	
	double velocityPIDf = 0.0;
	
	frc::SwerveDriveOdometry<4> m_odometry{m_kinematics, units::radian_t(imu.getYaw()),
	                                       frc::Pose2d{0_m, 0_m, 0_rad}};
//	frc::SwerveDrivePoseEstimator<4> estimator{
//			frc::Rotation2d(), frc::Pose2d(),
//			frc::MakeMatrix<5, 1>(0.01, 0.01, 0.01, 0.01, 0.01),
//			frc::MakeMatrix<3, 1>(0.1, 0.1, 0.1),
//			frc::MakeMatrix<3, 1>(0.1, 0.1, 0.1)};
};


#endif //BOTBUSTERS_REBIRTH_ECTOSWERVE_H
