//
// Created by andrew on 25/01/21.
//

//{1_mps, 3_mps, 1.5_rad_per_s}

#include "EctoSwerve.h"
#include "frc/smartdashboard/SmartDashboard.h"

EctoSwerve::EctoSwerve() {
	
	PIDConfig velocityPIDConfig;
	velocityPIDConfig.p = 3.0;
	velocityPIDf = 0.15;
	
	for (const auto &motor: velocityMotors) {
		motor->setEncoderCodesPerRev(42);
		motor->setPIDConfig(velocityPIDConfig);
		motor->setControlMode(MotorControlMode::Velocity);
		motor->setFeedbackMode(MotorFeedbackMode::QuadEncoder);
	}
	
	PIDConfig rotationPIDConfig;
	rotationPIDConfig.continous = true;
	rotationPIDConfig.maxInput = M_PI;
	rotationPIDConfig.minInput = -M_PI;
	rotationPIDConfig.p = 2.3;
	rotationPIDConfig.d = 0.1109;
	for (const auto &motorR: rotationMotors) {
		motorR->setAnalogPositionConversionFactor(5.0 / (2.0 * M_PI));
		motorR->setPIDConfig(rotationPIDConfig);
		motorR->setControlMode(MotorControlMode::Position);
		motorR->setFeedbackMode(MotorFeedbackMode::Potentiometer);
	}
	
}

void EctoSwerve::setVelocity(double vx, double vy, double omega, bool fieldOriented) {
	frc::ChassisSpeeds chassisVelocities;
	chassisVelocities.vx = units::meters_per_second_t(vx);
	chassisVelocities.vy = units::meters_per_second_t(vy);
	chassisVelocities.omega = units::radians_per_second_t(omega);
	
	if (fieldOriented) {
		chassisVelocities = frc::ChassisSpeeds::FromFieldRelativeSpeeds(chassisVelocities.vx, chassisVelocities.vy,
		                                                                chassisVelocities.omega,
		                                                                units::radian_t(imu.getYaw()));
	}
	
	auto[fl, fr, bl, br] = m_kinematics.ToSwerveModuleStates(chassisVelocities);
	float reducWheel = 8.31;
	
	double convertionSpeed1 = fr.speed.to<double>() / (wheelRadius * 2.0 * M_PI) * reducWheel;
	velocityMotors[0]->set(convertionSpeed1);
	velocityMotors[0]->setArbitraryFeedForward(convertionSpeed1 * velocityPIDf);
	
	frc::SmartDashboard::PutNumber("RequiredVelocityFRV", convertionSpeed1);
	
	double convertionSpeed2 = fl.speed.to<double>() / (wheelRadius * 2.0 * M_PI) * reducWheel;
	velocityMotors[1]->set(convertionSpeed2);
	velocityMotors[1]->setArbitraryFeedForward(convertionSpeed2 * velocityPIDf);
	
	double convertionSpeed3 = br.speed.to<double>() / (wheelRadius * 2.0 * M_PI) * reducWheel;
	velocityMotors[2]->set(convertionSpeed3);
	velocityMotors[2]->setArbitraryFeedForward(convertionSpeed3 * velocityPIDf);
	
	double convertionSpeed4 = bl.speed.to<double>() / (wheelRadius * 2.0 * M_PI) * reducWheel;
	velocityMotors[3]->set(convertionSpeed4);
	velocityMotors[3]->setArbitraryFeedForward(convertionSpeed4 * velocityPIDf);
	
	auto convertionRotation1 = fr.angle.Radians().to<double>();
	rotationMotors[0]->set(convertionRotation1);
	
	
	auto convertionRotation2 = fl.angle.Radians().to<double>();
	rotationMotors[1]->set(convertionRotation2);
	
	auto convertionRotation3 = br.angle.Radians().to<double>();
	rotationMotors[2]->set(convertionRotation3);
	
	auto convertionRotation4 = bl.angle.Radians().to<double>();
	rotationMotors[3]->set(convertionRotation4);
	
}

frc::ChassisSpeeds EctoSwerve::getVelocity(bool fieldOriented) {
	
	frc::ChassisSpeeds chassisVelocities = m_kinematics.ToChassisSpeeds(
			frontRightModuleState, backRightModuleState, frontLeftModuleState, backLeftModuleState);
	
	if (fieldOriented) {
		chassisVelocities = frc::ChassisSpeeds::FromFieldRelativeSpeeds(chassisVelocities.vx, chassisVelocities.vy,
		                                                                chassisVelocities.omega,
		                                                                units::radian_t(imu.getYaw()));
	}
	
	return chassisVelocities;
}

void EctoSwerve::update() {
	frontRightModuleState.speed = units::meters_per_second_t(velocityMotors[0]->getVelocity() * 2.0 * M_PI* wheelRadius / 8.31);
	frontRightModuleState.angle = units::radian_t(rotationMotors[0]->getPosition());
	
	frontLeftModuleState.speed = units::meters_per_second_t(velocityMotors[1]->getVelocity() * 2.0 * M_PI* wheelRadius / 8.31);
	frontLeftModuleState.angle = units::radian_t(rotationMotors[1]->getPosition());
	
	backRightModuleState.speed = units::meters_per_second_t(velocityMotors[2]->getVelocity() * 2.0 * M_PI* wheelRadius / 8.31);
	backRightModuleState.angle = units::radian_t(rotationMotors[2]->getPosition());
	
	backLeftModuleState.speed = units::meters_per_second_t(velocityMotors[3]->getVelocity() * 2.0 * M_PI* wheelRadius / 8.31);
	backLeftModuleState.angle = units::radian_t(rotationMotors[3]->getPosition());
	
	frc::Rotation2d gyroAngle{units::radian_t(imu.getYaw())};
	
	pose = m_odometry.Update(gyroAngle, frontLeftModuleState, frontRightModuleState,
	                         backLeftModuleState, backRightModuleState);
	
	frc::SmartDashboard::PutNumber("OdometryX", pose.X().to<double>());
	frc::SmartDashboard::PutNumber("OdometryY", pose.Y().to<double>());
	frc::SmartDashboard::PutNumber("OdometryOmega", pose.Rotation().Radians().to<double>());
	
}
frc::Pose2d EctoSwerve::getPose(){
	return pose;
}
