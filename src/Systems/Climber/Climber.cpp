//
// Created by rodrigoquintero    on 25/01/21.
//

#include "Climber.h"
#include "frc/Timer.h"

Climber::Climber()  {
	leftMotor = std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason","ClimberLeft");
	rightMotor = std::make_shared<EctoGazeboMotor>("Slimer2020_Offseason","ClimberRight");
	leftPiston = std::make_shared<EctoGazeboPiston>("Slimer2020_Offseason", "ClimberPistonLeft");
	rightPiston = std::make_shared<EctoGazeboPiston>("Slimer2020_Offseason","ClimberPistonRight");
}
void Climber::extendArms() {
	leftMotor->set(1);
	rightMotor->set(1);
}
void Climber::extendPiston() {
	leftPiston->setState(false);
	rightPiston->setState(false);
}
void Climber::retractArms() {
	leftMotor->set(.0);
	rightMotor->set(.0);
}
void Climber::retractPiston() {
	leftPiston->setState(true);
	rightPiston->setState(true);
}

void Climber::climbState() {
	wantToClimb = true;
}
void Climber::climbStateDown() {
	wantToDescend = true;
}
void Climber::stayUpState() {
	stayUp = true;
}

void Climber::update() {
	switch (currentState) {
		case ClimberStates::BeginningPosition:{
			if (wantToClimb){
				extendPiston();
				time = frc::Timer::GetFPGATimestamp();
				currentState = ClimberStates::ActionedPistons;
			}
			break;
		};
		case ClimberStates::ActionedPistons: {
			timePassed = (frc::Timer::GetFPGATimestamp() - time) > pistonWaitTimeUp;
			if (timePassed){
				currentState = ClimberStates::PistonsReady;
			}
			break;
		}
		case ClimberStates::PistonsReady:{
			extendArms();
			currentState = ClimberStates::ArmsUp;
			break;
		}
		case ClimberStates::ArmsUp: {
			leftMotorPosition = leftMotor->getPosition();
			rightMotorPosition = rightMotor->getPosition();
			if (leftMotorPosition > 1570 and rightMotorPosition > 1570) {
				motorAtPosition = true;
			}
			currentState = ClimberStates::ClimbingPosition;
			currentState = ClimberStates::Descend;
			break;
		}
		
		case ClimberStates::ClimbingPosition:{
			if(wantToDescend){
				retractArms();
			}
			rightMotorPositionDown = leftMotor->getPosition();
			leftMotorPositionDown = rightMotor->getPosition();
			if (leftMotorPositionDown < 0.5 and rightMotorPositionDown < 0.5){
				retractPiston();
			}
			break;
		}
		case ClimberStates::Descend:{
			if(stayUp == true){
				retractArms();
			}
			break;
		}
	}
}