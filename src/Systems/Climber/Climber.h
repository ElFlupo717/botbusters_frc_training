//
// Created by rodrigoquintero on 25/01/21.
//

#ifndef BOTBUSTERS_REBIRTH_CLIMBER_H
#define BOTBUSTERS_REBIRTH_CLIMBER_H
#include <memory>
#include "Core/MotorHandler/EctoMotor/EctoGazeboMotor.h"
#include "Core/PCM/EctoPiston/EctoGazeboPiston.h"

class Climber {
public:
	Climber();
	void extendArms();
	void extendPiston();
	void retractArms();
	void retractPiston();
	void update();
	void climbState();
	void climbStateDown();
	void stayUpState();
	
	enum class ClimberStates{
		BeginningPosition,
		ActionedPistons,
		PistonsReady,
		ArmsUp,
		ClimbingPosition,
		Descend
	};
private:
	std::shared_ptr<EctoMotor> leftMotor;
	std::shared_ptr<EctoMotor> rightMotor;
	std::shared_ptr<EctoGazeboPiston> rightPiston;
	std::shared_ptr<EctoGazeboPiston> leftPiston;
	ClimberStates currentState = ClimberStates::BeginningPosition;
	bool wantToClimb = false;
	bool wantToDescend = false;
	bool timePassed = false;
	bool motorAtPosition = false;
	double time = 0.0;
	const double pistonWaitTimeUp = 3;
	double leftMotorPosition;
	double rightMotorPosition;
	double leftMotorPositionDown;
	double rightMotorPositionDown;
	bool stayUp = false;
	
};

#endif //BOTBUSTERS_REBIRTH_CLIMBER_H
