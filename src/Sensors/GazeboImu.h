//
// Created by ajahueym on 1/30/21.
//

#ifndef BOTBUSTERS_REBIRTH_GAZEBOIMU_H
#define BOTBUSTERS_REBIRTH_GAZEBOIMU_H
#include <string>
#include <networktables/NetworkTable.h>
#include <networktables/NetworkTableInstance.h>

class GazeboIMU {
public:
	GazeboIMU(const std::string& robotName, const std::string& imuName);
	double getRoll();
	double getPitch();
	double getYaw();
private:
	nt::NetworkTableInstance ntInstance = nt::NetworkTableInstance::GetDefault();
	std::shared_ptr<nt::NetworkTable> imuTable;
	nt::NetworkTableEntry roll, pitch, yaw;
};


#endif //BOTBUSTERS_REBIRTH_GAZEBOIMU_H
