//
// Created by abiel on 7/14/20.
//

#include "GazeboRobot.h"


GazeboRobot::GazeboRobot() : EctoRobot("GazeboRobot") {
}

void GazeboRobot::robotInit() {
}

void GazeboRobot::robotUpdate() {
}

void GazeboRobot::disabledInit() {

}

void GazeboRobot::disabledUpdate() {

}

void GazeboRobot::autoInit() {
}

void GazeboRobot::autoUpdate() {
}

void GazeboRobot::teleopInit() {
}

void GazeboRobot::teleopUpdate() {
}

