//
// Created by alberto on 31/07/19.
//
#ifndef ECTOCONTROL_MOTORHANDLER_H
#define ECTOCONTROL_MOTORHANDLER_H

#include "Core/MotorHandler/EctoMotor/EctoMotor.h"
#include "Core/EctoModule/Manager.h"

class MotorManager : public Manager<MotorManager> {
	friend class Manager<MotorManager>;

public:
	void setMotors(std::map<std::string, std::shared_ptr<EctoMotor>> motors);
	std::shared_ptr<EctoMotor> &getMotor(const std::string &name);

protected:
	void update() override;

private:
	
	std::map<std::string, std::shared_ptr<EctoMotor>> motorControllers;
	std::map<std::string, bool> motorTaken;
	
	MotorManager();
};

#endif //ECTOCONTROL_MOTORHANDLER_H