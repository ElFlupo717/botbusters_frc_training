//
// Created by alberto on 31/07/19.
//

#include "MotorManager.h"
#include <iostream>
#include <utility>


MotorManager::MotorManager() : Manager("MotorHandler") {
	log->info("Initializing EctoMotors...");
}

void MotorManager::setMotors(std::map<std::string, std::shared_ptr<EctoMotor>> motors) {
	motorControllers = std::move(motors);
}


void MotorManager::update() {
	;
}

std::shared_ptr<EctoMotor> &MotorManager::getMotor(const std::string &name) {
	if(motorControllers.count(name) != 0){
		if(motorTaken[name]){
			log->warn("Motor with name {}, was retrieved more than once! Some systems may be using the same motor instance!", name);
		}else{
			motorTaken[name] = true;
		}
		
		return motorControllers[name];
	}
	
	throw std::invalid_argument("Could not get motor with the name '" + name + "', it does not exist");
}
