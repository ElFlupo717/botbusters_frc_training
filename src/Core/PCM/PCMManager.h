//
// Created by abiel on 8/22/19.
//

#ifndef BOTBUSTERSREBIRTH_ECTOPCM_H
#define BOTBUSTERSREBIRTH_ECTOPCM_H

#include <frc/Solenoid.h>
#include <Core/EctoModule/Manager.h>
#include <Core/PCM/EctoPiston/EctoPiston.h>
#include <string>
#include <map>


class PCMManager : public Manager<PCMManager> {
	friend class Manager<PCMManager>;

public:
	void setPistons(std::map<std::string, std::shared_ptr<EctoPiston>> pistons);
	
	std::shared_ptr<EctoPiston> &getPiston(const std::string &name);
	
protected:
	void update() override;

private:
	
	std::map<std::string, std::shared_ptr<EctoPiston>> pistonsControllers;
	std::map<std::string, bool> pistonTaken;
	
	PCMManager();
};

#endif //BOTBUSTERSREBIRTH_ECTOPCM_H
