//
// Created by abiel on 8/22/19.
//

#include "PCMManager.h"

PCMManager::PCMManager() : Manager("PCMManager") {
	log->info("Initializing PCMManager...");
	
}

void PCMManager::setPistons(std::map<std::string, std::shared_ptr<EctoPiston>> pistons) {
	pistonsControllers = std::move(pistons);
}

std::shared_ptr<EctoPiston> &PCMManager::getPiston(const std::string &name) {
	if(pistonsControllers.count(name) != 0){
		if(pistonTaken[name]){
			log->warn("Piston with name {}, was retrieved more than once! Some systems may be using the same piston instance!", name);
		}else{
			pistonTaken[name] = true;
		}
		
		return pistonsControllers[name];
	}
	
	throw std::invalid_argument("Could not get piston with the name '" + name + "', it does not exist");
}

void PCMManager::update() {
	;
}