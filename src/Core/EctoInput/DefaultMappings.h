//
// Created by ajahueym on 1/13/21.
//

#ifndef BOTBUSTERS_REBIRTH_DEFAULTMAPPINGS_H
#define BOTBUSTERS_REBIRTH_DEFAULTMAPPINGS_H
#include "InputManager.h"

namespace EctoInput{
	struct DefaultMappings{
		static InputManager::JoystickMap Xbox(){
			InputManager::JoystickMap xbox;
			xbox.buttonsMapping = {
					{"A", 0},
					{"B", 1},
					{"X", 2},
					{"Y", 3},
					{"leftBumper", 4},
					{"rightBumper", 5},
					{"select", 6},
					{"start", 7},
					{"home", 8},
					{"leftJoystick", 9},
					{"rightJoystick", 10}
			};
			
			xbox.axesMapping = {
					{"leftX", 0},
					{"leftY", 1},
					{"leftTrigger", 2},
					{"rightX", 3},
					{"rightY", 4},
					{"rightTrigger", 5}
			};
			
			return xbox;
		}

	};
}
#endif //BOTBUSTERS_REBIRTH_DEFAULTMAPPINGS_H
