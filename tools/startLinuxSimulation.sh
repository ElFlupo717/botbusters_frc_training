#!/usr/bin/env bash

OS=$(uname -s)
if [ "$OS" == "Linux" ];
then
  ./gradlew simulateFrcUserProgramLinuxx86-64DebugExecutable
  PROGRAM_INFO=$?
  if [ "$PROGRAM_INFO" == "1" ]; then
    exit 1
  fi

  PID=$(cat build/pids/simulateFrcUserProgramLinuxx86-64DebugExecutable.pid)
  # shellcheck disable=SC2086
  tail --pid=$PID -f build/stdout/simulateFrcUserProgramLinuxx86-64DebugExecutable.log
  exit 0
else
  echo "This script is meant for Linux systems only!" >&2
  exit 1
fi