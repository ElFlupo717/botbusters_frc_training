#!/usr/bin/env bash

OS=$(uname -s)
if [ "$OS" == "Linux" ];
then
  ./gradlew frcUserProgramLinuxx86-64DebugExecutable
  PROGRAM_INFO=$?
  if [ "$PROGRAM_INFO" == "1" ]; then
    exit 1
  fi

  scp  build/exe/frcUserProgram/linuxx86-64/debug/frcUserProgram "$USER"@10.0.0.1:~/remoteSimulate
  exit 0
else
  echo "This script is meant for Linux systems only!" >&2
  exit 1
fi