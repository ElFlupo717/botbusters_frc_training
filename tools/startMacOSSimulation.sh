#!/usr/bin/env zsh

OS=$(uname -s)
if [[ "$OS" == "Darwin" ]];
then
  ./gradlew simulateFrcUserProgramOsxx86-64DebugExecutable

  PROGRAM_INFO=$?
  if [[ "$PROGRAM_INFO" == "1" ]]; then
    exit 1
  fi

  PID=$(cat build/pids/simulateFrcUserProgramOsxx86-64DebugExecutable.pid)
  # shellcheck disable=SC2086
  tail --pid=$PID -f build/stdout/simulateFrcUserProgramOsxx86-64DebugExecutable.log
  exit 1
else
  echo "This script is meant for Mac OS systems only!" >&2
  exit 0
fi